/*  Slideshow°° JS
	2024
	@sacaix
	arnAu bellavista
	www.kipple.net
*/
// JS que s'execuarà un cop carregat el document
window.onload = function(){
	
	var slide00 = document.getElementsByClassName("slide00");
	var slides00 = [];

	class Slide00{
		constructor(element){
			this.element = element;
			this.slides = [];
			this.slideIndex = 0;
			this.slideAnt = 0;
			this.slidePrimer = true;
			this.interval;
			this.intervalValor = 0;		// 0 segons per defecte
			this.fos = false;			// Sense fos entre slides per defecte
			this.slideNav = false;		// No per defecte
			this.slidePunts = false;	// No per defecte
			this.punts = [];
			this.crea();
		}
		crea(){
			var self = this;
			// Navegació d'slides
			if(this.element.getAttribute("nav") && this.element.getAttribute("nav") == 'true'){
				this.slideNav = true;
			}
			// Navegació per punts
			if(this.element.getAttribute("dots") && this.element.getAttribute("dots") == 'true'){
				this.slidePunts = true;
			}
			// Intèrval en milisegons
			if(this.element.getAttribute("interval") && this.element.getAttribute("interval") > 0){
				this.intervalValor = this.element.getAttribute("interval") * 1000; // Agafem i passem a milisegons
			}
			// Overlap: fos entre slides
			if(this.element.getAttribute("fos") && this.element.getAttribute("fos")=='true'){
				this.fos = true;
			}
			// Agafem les capes dels slides que són dins de la capa .slide00
			// No ho agafem així (this.slides = this.element.children) perquè sinó agafa també les capes que creem més endavant
			for(var i=0; i<this.element.children.length; i++){
				this.slides.push(this.element.children[i]);
			}

			// Recorrem els slides i muntem les capes
			for(let i=0; i<this.slides.length; i++){
				// Agafem la imatge
				if(this.fos){
					this.slides[i].setAttribute("class", "slide00s slide00fosEntrada");
				}else{
					this.slides[i].setAttribute("class", "slide00s");
				}
				this.slides[i].setAttribute("style", "background: url('"+this.slides[i].getAttribute("imatge")+"'); background-size:cover; background-position:center;");
				// Agafem enllaç
				if(this.slides[i].getAttribute("link")){
					this.slides[i].setAttribute("onclick", "window.location='"+this.slides[i].getAttribute("link")+"';");
					this.slides[i].setAttribute("style", "background: url('"+this.slides[i].getAttribute("imatge")+"'); background-size:cover; background-position:center; cursor:pointer;");
					this.slides[i].removeAttribute("link"); // Eliminem l'atribut inicial
				}
				this.slides[i].removeAttribute("imatge"); // Eliminem l'atribut inicial
			}
			
			// Mostrem les capes de navegació si es vol
			if(this.slideNav){
				// Creem les capes de navegació
				let enrere = document.createElement("div");
					enrere.setAttribute("class", "slide00Anterior");
					// enrere.setAttribute("onclick", "segSlide(-1)");
					enrere.innerHTML = "<span class='slide00Vmig'>&#10094;</span>";
					this.element.appendChild(enrere);
				let endavant = document.createElement("div");
					endavant.setAttribute("class", "slide00Seguent");
					// enrere.setAttribute("onclick", "segSlide(1)");
					endavant.innerHTML = "<span class='slide00Vmig slide00VmigD'>&#10095;</span>";
					this.element.appendChild(endavant);
				// Eseveniments clicables
				enrere.onclick = function(){
					var s = self.slideIndex - 1;
					self.nouSlide(self.slideIndex - 1);
					self.intervalZero();
				};
				endavant.onclick = function(){
					var s = self.slideIndex + 1;
					self.nouSlide(self.slideIndex + 1);
					self.intervalZero();
				};
			}
			
			// Creem les navegació per punts
			if(this.slidePunts){
				// Creem la capa d'slide amb la imatge
				let slidePunts00 = document.createElement("div");
				slidePunts00.setAttribute("class", "punts");
				this.element.appendChild(slidePunts00);
				
				for(let i=0; i<this.slides.length; i++){
					let punt = document.createElement("span");
						punt.setAttribute("class", "punt");
						punt.setAttribute("onclick", "aqSlide("+i+")");
					slidePunts00.appendChild(punt);
					this.punts[i] = punt;
					// Eseveniments clicables
					punt.onclick = function(){
						self.nouSlide(i);
						self.intervalZero();
					};
				}
			}

			// Mostrem el primer slide
			this.nouSlide(this.slideIndex);
			
			if(this.slides.length > 1){
				if(this.intervalValor > 0){
					// Interval per executar la consulta de noves dades
					var s = this.slideIndex+1;
					this.interval = setInterval(function(){
						self.nouSlide(s++);
					}, this.intervalValor);
				}
			}
		}
		
		nouSlide(s){
			this.slideAnt = this.slideIndex;
			if(s==-1){
				this.slideIndex = this.slides.length-1;
			}else{
				this.slideIndex = s % this.slides.length;
			}
			if(this.slidePrimer){
				this.slidePrimer = false;
			}else{
				if(this.fos){
					// Posem fos l'slide anterior
					this.slides[this.slideAnt].classList.add("slide00fosSortida");
					// Amaguem l'slide anterior al cap de 2 segons
					setTimeout(() => {
						this.slides[this.slideAnt].style.display = "none";
					}, 1500);
				}else{
					this.slides[this.slideAnt].style.display = "none";
				}
			}
			// Mostrem l'slide corresponent
			this.slides[this.slideIndex].classList.remove("slide00fosSortida");
			this.slides[this.slideIndex].style.display = "block";
			// Actualitzem els punts
			if(this.slidePunts){
				for(let i=0; i<this.punts.length; i++) {
					this.punts[i].classList.remove("puntActiu");
				}
				this.punts[this.slideIndex].classList.add("puntActiu");
			}
		}
		
		intervalZero(){
			if(this.intervalValor > 0){
				var self = this;
				// Reiniciem l'interval
				clearInterval(this.interval);
				var s = this.slideIndex + 1;
				this.interval = setInterval(function(){
					self.nouSlide(s++);
				}, this.intervalValor);
			}
		}
	}

	// Creem els slides si n'hi ha
	if(slide00){
		for(let i=0; i<slide00.length; i++){
			slides00.push(new Slide00(
				element = slide00[i]
			));
		}
	}

}